﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        [Required]
        [StringLength(100,ErrorMessage="Tối đa là 100 ký tự !!")]
        public String FirstName { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "Tối đa là 100 ký tự !!")]
        public String LastName { set; get; }

        //
        public ICollection<Post> Posts { set; get; }
        public ICollection<Comment> Comments { set; get; }
    }
}