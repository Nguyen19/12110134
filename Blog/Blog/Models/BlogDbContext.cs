﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class BlogDbContext:DbContext
    {
        public DbSet<Post> Post { set; get; }
        public DbSet<Comment> Comment { set; get; }
        public DbSet<Tag> Tag { set; get; }
        public DbSet<Account> Account { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().HasMany(t => t.Tags).WithMany(p => p.Posts)
                .Map(l => l.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Posts-Tags"));
            base.OnModelCreating(modelBuilder);
        }
    }
}