﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        public int PostID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Tối thiểu là 20 kí tự và tối đa là 500 kí tự !!", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [StringLength(int.MaxValue, ErrorMessage = "Tối thiểu là 50 kí tự !!", MinimumLength = 50)]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public String DateUpdated { set; get; }

        //
        public ICollection<Comment> Comments { set; get; }
        public ICollection<Tag> Tags { set; get; }
        public int AccountID { set; get; }
        public virtual Account Account { set; get; }


    }
}