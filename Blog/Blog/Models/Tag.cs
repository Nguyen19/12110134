﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required]
        [StringLength(100,ErrorMessage="Tối thiểu là 10 kí tự và tối đa là 100 kí tự !!",MinimumLength=10)]
        public String Content { set; get; }

        //
        public ICollection<Post> Posts { set; get; }
    }
}