﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        public int CommentID { set; get; }
        [Required]
        [StringLength(int.MaxValue, ErrorMessage = "Tối thiểu là 50 kí tự !!", MinimumLength = 50)]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        [Required]
        public String Author { set; get; }

        //
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        public virtual Account Account { set; get; }
    }
}