namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.String(),
                        AccountID = c.Int(nullable: false)
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.Accounts", t => t.AccountID, cascadeDelete: true)
                .Index(t => t.AccountID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        Author = c.String(nullable: false),
                        PostID = c.Int(nullable: false),
                        Account_AccountID = c.Int(),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Accounts", t => t.Account_AccountID)
                .Index(t => t.PostID)
                .Index(t => t.Account_AccountID);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.AccountID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Posts-Tags",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts-Tags", new[] { "TagID" });
            DropIndex("dbo.Posts-Tags", new[] { "PostID" });
            DropIndex("dbo.Comments", new[] { "Account_AccountID" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "AccountID" });
            DropForeignKey("dbo.Posts-Tags", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Posts-Tags", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "Account_AccountID", "dbo.Accounts");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "AccountID", "dbo.Accounts");
            DropTable("dbo.Posts-Tags");
            DropTable("dbo.Tags");
            DropTable("dbo.Accounts");
            DropTable("dbo.Comments");
            DropTable("dbo.Posts");
        }
    }
}
