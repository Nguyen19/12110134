namespace SonyPassionWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Threads", "ContentOfThread", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Threads", "ContentOfThread");
        }
    }
}
