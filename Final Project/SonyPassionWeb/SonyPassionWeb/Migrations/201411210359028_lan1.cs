namespace SonyPassionWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        NewsID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        DateUpLoad = c.DateTime(nullable: false),
                        Body = c.String(nullable: false),
                        SonyPassionID = c.Int(nullable: false),
                        XperiaID = c.Int(nullable: false),
                        SmartWatchID = c.Int(nullable: false),
                        HeadphonesEarbudsID = c.Int(nullable: false),
                        VideoID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NewsID)
                .ForeignKey("dbo.SonyPassions", t => t.SonyPassionID, cascadeDelete: true)
                .ForeignKey("dbo.Xperias", t => t.XperiaID, cascadeDelete: true)
                .ForeignKey("dbo.SmartWatches", t => t.SmartWatchID, cascadeDelete: true)
                .ForeignKey("dbo.HeadphonesEarbuds", t => t.HeadphonesEarbudsID, cascadeDelete: true)
                .ForeignKey("dbo.Videos", t => t.VideoID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.SonyPassionID)
                .Index(t => t.XperiaID)
                .Index(t => t.SmartWatchID)
                .Index(t => t.HeadphonesEarbudsID)
                .Index(t => t.VideoID)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.SonyPassions",
                c => new
                    {
                        SonyPassionID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SonyPassionID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        DateComment = c.DateTime(nullable: false),
                        Author = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        NewsID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.News", t => t.NewsID, cascadeDelete: true)
                .Index(t => t.NewsID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Xperias",
                c => new
                    {
                        XperiaID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.XperiaID);
            
            CreateTable(
                "dbo.SmartWatches",
                c => new
                    {
                        SmartWatchID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SmartWatchID);
            
            CreateTable(
                "dbo.HeadphonesEarbuds",
                c => new
                    {
                        HeadphonesEarbudsID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.HeadphonesEarbudsID);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        VideoID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.VideoID);
            
            CreateTable(
                "dbo.Threads",
                c => new
                    {
                        ThreadID = c.Int(nullable: false, identity: true),
                        TitleOfThread = c.String(nullable: false),
                        DateOfThread = c.DateTime(nullable: false),
                        LastTimeEdited = c.DateTime(nullable: false),
                        ForumID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ThreadID)
                .ForeignKey("dbo.Fora", t => t.ForumID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.ForumID)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Fora",
                c => new
                    {
                        ForumID = c.Int(nullable: false, identity: true),
                        ForumModel = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ForumID);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        Author = c.String(nullable: false),
                        ContentOfPost = c.String(),
                        DateOfPost = c.DateTime(nullable: false),
                        ThreadID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.Threads", t => t.ThreadID, cascadeDelete: true)
                .Index(t => t.ThreadID);
            
            CreateTable(
                "dbo.Specs",
                c => new
                    {
                        SpecID = c.Int(nullable: false, identity: true),
                        ModelName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.SpecID);
            
            CreateTable(
                "dbo.SpecDetails",
                c => new
                    {
                        SpecDetailsID = c.Int(nullable: false, identity: true),
                        Display = c.String(nullable: false),
                        Ram = c.String(nullable: false),
                        Internal = c.String(nullable: false),
                        Chipest = c.String(nullable: false),
                        Battery = c.String(nullable: false),
                        SpecID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SpecDetailsID)
                .ForeignKey("dbo.Specs", t => t.SpecID, cascadeDelete: true)
                .Index(t => t.SpecID);
            
            CreateTable(
                "dbo.News-Tag",
                c => new
                    {
                        NewsID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.NewsID, t.TagID })
                .ForeignKey("dbo.News", t => t.NewsID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.NewsID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.News-Tag", new[] { "TagID" });
            DropIndex("dbo.News-Tag", new[] { "NewsID" });
            DropIndex("dbo.SpecDetails", new[] { "SpecID" });
            DropIndex("dbo.Posts", new[] { "ThreadID" });
            DropIndex("dbo.Threads", new[] { "UserProfileUserId" });
            DropIndex("dbo.Threads", new[] { "ForumID" });
            DropIndex("dbo.Comments", new[] { "NewsID" });
            DropIndex("dbo.News", new[] { "UserProfileUserId" });
            DropIndex("dbo.News", new[] { "VideoID" });
            DropIndex("dbo.News", new[] { "HeadphonesEarbudsID" });
            DropIndex("dbo.News", new[] { "SmartWatchID" });
            DropIndex("dbo.News", new[] { "XperiaID" });
            DropIndex("dbo.News", new[] { "SonyPassionID" });
            DropForeignKey("dbo.News-Tag", "TagID", "dbo.Tags");
            DropForeignKey("dbo.News-Tag", "NewsID", "dbo.News");
            DropForeignKey("dbo.SpecDetails", "SpecID", "dbo.Specs");
            DropForeignKey("dbo.Posts", "ThreadID", "dbo.Threads");
            DropForeignKey("dbo.Threads", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Threads", "ForumID", "dbo.Fora");
            DropForeignKey("dbo.Comments", "NewsID", "dbo.News");
            DropForeignKey("dbo.News", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.News", "VideoID", "dbo.Videos");
            DropForeignKey("dbo.News", "HeadphonesEarbudsID", "dbo.HeadphonesEarbuds");
            DropForeignKey("dbo.News", "SmartWatchID", "dbo.SmartWatches");
            DropForeignKey("dbo.News", "XperiaID", "dbo.Xperias");
            DropForeignKey("dbo.News", "SonyPassionID", "dbo.SonyPassions");
            DropTable("dbo.News-Tag");
            DropTable("dbo.SpecDetails");
            DropTable("dbo.Specs");
            DropTable("dbo.Posts");
            DropTable("dbo.Fora");
            DropTable("dbo.Threads");
            DropTable("dbo.Videos");
            DropTable("dbo.HeadphonesEarbuds");
            DropTable("dbo.SmartWatches");
            DropTable("dbo.Xperias");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.SonyPassions");
            DropTable("dbo.News");
            DropTable("dbo.UserProfile");
        }
    }
}
