﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class SpecDetails
    {
        public int SpecDetailsID { get; set; }
        [Required]
        public String Display { get; set; }
        [Required]
        public String Ram { get; set; }
        [Required]
        public String Internal { get; set; }
        [Required]
        public String Chipest { get; set; }
        [Required]
        public String Battery { get; set; }

        //
        public int SpecID { get; set; }
        public virtual Spec Spec { get; set; }
    }
}