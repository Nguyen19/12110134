﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class Post
    {
        public int PostID { get; set; }
        [Required]
        public String Author { get; set; }
        [StringLength(int.MaxValue, ErrorMessage = "Nội dung phải chứa ít nhất 4 ký tự !!", MinimumLength = 4)]
        public String ContentOfPost { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateOfPost { get; set; }

        //
        public int LastPost
        {
            get
            {
                return (DateTime.Now - DateOfPost).Minutes;
            }
        }

        //
        public int ThreadID { get; set; }
        public virtual Thread Thread { get; set; }
    }
}