﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class SonyPassionDbContext : DbContext
    {
        public SonyPassionDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<SonyPassion> SonyPassions { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Xperia> Xperia { set; get; }
        public DbSet<HeadphonesEarbuds> HeadphonesEarbuds { set; get; }
        public DbSet<SmartWatch> SmartWatch { set; get; }
        public DbSet<Video> Video { set; get; }
        public DbSet<Comment> Comment { set; get; }
        public DbSet<Tag> Tag { set; get; }
        public DbSet<Forum> Forum { get; set; }
        public DbSet<Thread> Thread { set; get; }
        public DbSet<Post> Post { set; get; }
        public DbSet<Spec> Spec { get; set; }
        public DbSet<SpecDetails> SpecDetails { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<News>().HasMany(t => t.Tags).WithMany(n => n.News)
               .Map(l => l.MapLeftKey("NewsID").MapRightKey("TagID").ToTable("News-Tag"));

            base.OnModelCreating(modelBuilder);
        }
    }
}