﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class News
    {
        public int NewsID { get; set; }
        [Required]
        public string Title { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpLoad { get; set; }
        [Required]
        public String Body { get; set; }

        //
        public int SonyPassionID { get; set; }
        public virtual SonyPassion SonyPassion { get; set; }
        //
        public virtual ICollection<Comment> Comments { get; set; }
        //
        public virtual ICollection<Tag> Tags { get; set; }
        //
        public int XperiaID { get; set; }
        public virtual Xperia Xperia { get; set; }
        //
        public int SmartWatchID { get; set; }
        public virtual SmartWatch SmartWatch { get; set; }
        //
        public int HeadphonesEarbudsID { get; set; }
        public virtual HeadphonesEarbuds HeadphonesEarbuds { get; set; }
        //
        public int VideoID { get; set; }
        public virtual Video Video { get; set; }
        //
        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}