﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class Spec
    {
        public int SpecID { get; set; }
        [Required]
        public String ModelName { get; set; }

        public virtual ICollection<SpecDetails> SpecDetails { get; set; }
    }
}