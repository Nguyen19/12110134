﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateComment { get; set; }
        [Required]
        public String Author { get; set; }
        [Required]
        [StringLength(int.MaxValue, ErrorMessage = "Nội dung phải chứa ít nhất 4 ký tự !!", MinimumLength = 4)]
        public String Content { get; set; }

        //
        public int LastComment
        {
            get
            {
                return (DateTime.Now - DateComment).Minutes;
            }
        }

        //
        public int NewsID { get; set; }
        public virtual News News { get; set; }
    }
}