﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class Forum
    {
        public int ForumID { get; set; }
        [Required]
        public String ForumModel { get; set; }

        //
        public virtual ICollection<Thread> Thread { get; set; }
    }
}