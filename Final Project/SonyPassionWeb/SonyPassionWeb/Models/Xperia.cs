﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class Xperia
    {
        public int XperiaID { get; set; }

        //
        public virtual ICollection<News> News { get; set; }
    }
}