﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class Thread
    {
        public int ThreadID { get; set; }
        [Required]
        public String TitleOfThread { get; set; }
        [Required]
        public String ContentOfThread { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateOfThread { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime LastTimeEdited { get; set; }
        

        //
        public int ForumID { get; set; }
        public virtual Forum Forum { get; set; }
        //
        public virtual ICollection<Post> Post { get; set; }
        //
        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}