﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyPassionWeb.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        [Required]
        public String Content { get; set; }

        //
        public virtual ICollection<News> News { get; set; }
    }
}