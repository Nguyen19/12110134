﻿$(function () {
    $("#my-form1").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (data) {
                $("#result1").prepend(data);
            }
        });
        $("#my-form1").trigger("reset");
    });
});