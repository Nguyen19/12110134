﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    [Authorize]
    public class ForumController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /Forum/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Forum.ToList());
        }

        //
        // GET: /Forum/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Forum forum = db.Forum.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        //
        // GET: /Forum/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Forum/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Forum forum)
        {
            if (ModelState.IsValid)
            {
                db.Forum.Add(forum);              
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(forum);
        }

        //
        // GET: /Forum/Edit/5
        
        public ActionResult Edit(int id = 0)
        {
            Forum forum = db.Forum.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        //
        // POST: /Forum/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Forum forum)
        {
            if (ModelState.IsValid)
            {
                db.Entry(forum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(forum);
        }

        //
        // GET: /Forum/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Forum forum = db.Forum.Find(id);
            if (forum == null)
            {
                return HttpNotFound();
            }
            return View(forum);
        }

        //
        // POST: /Forum/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Forum forum = db.Forum.Find(id);
            db.Forum.Remove(forum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}