﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /News/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var news = db.News.Include(n => n.SonyPassion).Include(n => n.Xperia).Include(n => n.SmartWatch).Include(n => n.HeadphonesEarbuds).Include(n => n.Video).Include(n => n.UserProfile);
            return View(news.ToList());
        }

        //
        // GET: /News/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            News news = db.News.Find(id);
            ViewData["idnews"] = id;
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        //
        // GET: /News/Create

        public ActionResult Create()
        {
            ViewBag.SonyPassionID = new SelectList(db.SonyPassions, "SonyPassionID", "SonyPassionID");
            ViewBag.XperiaID = new SelectList(db.Xperia, "XperiaID", "XperiaID");
            ViewBag.SmartWatchID = new SelectList(db.SmartWatch, "SmartWatchID", "SmartWatchID");
            ViewBag.HeadphonesEarbudsID = new SelectList(db.HeadphonesEarbuds, "HeadphonesEarbudsID", "HeadphonesEarbudsID");
            ViewBag.VideoID = new SelectList(db.Video, "VideoID", "VideoID");
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /News/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(News news, string Content)
        {
            if (ModelState.IsValid)
            {
                news.DateUpLoad = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                news.UserProfileUserId = userid;
                List<Tag> Tags = new List<Tag>();
                string[] TagContent = Content.Split(',');
                foreach (string item in TagContent)
                {
                    Tag tagExits = null;
                    var ListTag = db.Tag.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        tagExits = ListTag.First();
                        tagExits.News.Add(news);
                    }
                    else
                    {
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.News = new List<News>();
                        tagExits.News.Add(news);
                    }
                    Tags.Add(tagExits);
                }
                news.Tags = Tags;
                db.News.Add(news);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SonyPassionID = new SelectList(db.SonyPassions, "SonyPassionID", "SonyPassionID", news.SonyPassionID);
            ViewBag.XperiaID = new SelectList(db.Xperia, "XperiaID", "XperiaID", news.XperiaID);
            ViewBag.SmartWatchID = new SelectList(db.SmartWatch, "SmartWatchID", "SmartWatchID", news.SmartWatchID);
            ViewBag.HeadphonesEarbudsID = new SelectList(db.HeadphonesEarbuds, "HeadphonesEarbudsID", "HeadphonesEarbudsID", news.HeadphonesEarbudsID);
            ViewBag.VideoID = new SelectList(db.Video, "VideoID", "VideoID", news.VideoID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", news.UserProfileUserId);
            return View(news);
        }

        //
        // GET: /News/Edit/5

        public ActionResult Edit(int id = 0)
        {
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.SonyPassionID = new SelectList(db.SonyPassions, "SonyPassionID", "SonyPassionID", news.SonyPassionID);
            ViewBag.XperiaID = new SelectList(db.Xperia, "XperiaID", "XperiaID", news.XperiaID);
            ViewBag.SmartWatchID = new SelectList(db.SmartWatch, "SmartWatchID", "SmartWatchID", news.SmartWatchID);
            ViewBag.HeadphonesEarbudsID = new SelectList(db.HeadphonesEarbuds, "HeadphonesEarbudsID", "HeadphonesEarbudsID", news.HeadphonesEarbudsID);
            ViewBag.VideoID = new SelectList(db.Video, "VideoID", "VideoID", news.VideoID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", news.UserProfileUserId);
            return View(news);
        }

        //
        // POST: /News/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(News news)
        {
            if (ModelState.IsValid)
            {
                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SonyPassionID = new SelectList(db.SonyPassions, "SonyPassionID", "SonyPassionID", news.SonyPassionID);
            ViewBag.XperiaID = new SelectList(db.Xperia, "XperiaID", "XperiaID", news.XperiaID);
            ViewBag.SmartWatchID = new SelectList(db.SmartWatch, "SmartWatchID", "SmartWatchID", news.SmartWatchID);
            ViewBag.HeadphonesEarbudsID = new SelectList(db.HeadphonesEarbuds, "HeadphonesEarbudsID", "HeadphonesEarbudsID", news.HeadphonesEarbudsID);
            ViewBag.VideoID = new SelectList(db.Video, "VideoID", "VideoID", news.VideoID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", news.UserProfileUserId);
            return View(news);
        }

        //
        // GET: /News/Delete/5

        public ActionResult Delete(int id = 0)
        {
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        //
        // POST: /News/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            News news = db.News.Find(id);
            db.News.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}