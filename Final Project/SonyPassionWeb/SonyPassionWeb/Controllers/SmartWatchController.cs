﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    public class SmartWatchController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /SmartWatch/

        public ActionResult Index()
        {
            return View(db.SmartWatch.ToList());
        }

        //
        // GET: /SmartWatch/Details/5

        public ActionResult Details(int id = 0)
        {
            SmartWatch smartwatch = db.SmartWatch.Find(id);
            if (smartwatch == null)
            {
                return HttpNotFound();
            }
            return View(smartwatch);
        }

        //
        // GET: /SmartWatch/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SmartWatch/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SmartWatch smartwatch)
        {
            if (ModelState.IsValid)
            {
                db.SmartWatch.Add(smartwatch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(smartwatch);
        }

        //
        // GET: /SmartWatch/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SmartWatch smartwatch = db.SmartWatch.Find(id);
            if (smartwatch == null)
            {
                return HttpNotFound();
            }
            return View(smartwatch);
        }

        //
        // POST: /SmartWatch/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SmartWatch smartwatch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(smartwatch).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(smartwatch);
        }

        //
        // GET: /SmartWatch/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SmartWatch smartwatch = db.SmartWatch.Find(id);
            if (smartwatch == null)
            {
                return HttpNotFound();
            }
            return View(smartwatch);
        }

        //
        // POST: /SmartWatch/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SmartWatch smartwatch = db.SmartWatch.Find(id);
            db.SmartWatch.Remove(smartwatch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}