﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    public class HeadphonesEarbudsController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /HeadphonesEarbuds/

        public ActionResult Index()
        {
            return View(db.HeadphonesEarbuds.ToList());
        }

        //
        // GET: /HeadphonesEarbuds/Details/5

        public ActionResult Details(int id = 0)
        {
            HeadphonesEarbuds headphonesearbuds = db.HeadphonesEarbuds.Find(id);
            if (headphonesearbuds == null)
            {
                return HttpNotFound();
            }
            return View(headphonesearbuds);
        }

        //
        // GET: /HeadphonesEarbuds/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /HeadphonesEarbuds/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HeadphonesEarbuds headphonesearbuds)
        {
            if (ModelState.IsValid)
            {
                db.HeadphonesEarbuds.Add(headphonesearbuds);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(headphonesearbuds);
        }

        //
        // GET: /HeadphonesEarbuds/Edit/5

        public ActionResult Edit(int id = 0)
        {
            HeadphonesEarbuds headphonesearbuds = db.HeadphonesEarbuds.Find(id);
            if (headphonesearbuds == null)
            {
                return HttpNotFound();
            }
            return View(headphonesearbuds);
        }

        //
        // POST: /HeadphonesEarbuds/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HeadphonesEarbuds headphonesearbuds)
        {
            if (ModelState.IsValid)
            {
                db.Entry(headphonesearbuds).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(headphonesearbuds);
        }

        //
        // GET: /HeadphonesEarbuds/Delete/5

        public ActionResult Delete(int id = 0)
        {
            HeadphonesEarbuds headphonesearbuds = db.HeadphonesEarbuds.Find(id);
            if (headphonesearbuds == null)
            {
                return HttpNotFound();
            }
            return View(headphonesearbuds);
        }

        //
        // POST: /HeadphonesEarbuds/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HeadphonesEarbuds headphonesearbuds = db.HeadphonesEarbuds.Find(id);
            db.HeadphonesEarbuds.Remove(headphonesearbuds);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}