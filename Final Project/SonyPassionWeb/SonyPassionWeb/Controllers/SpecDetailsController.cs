﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    [Authorize]
    public class SpecDetailsController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /SpecDetails/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var specdetails = db.SpecDetails.Include(s => s.Spec);
            return View(specdetails.ToList());
        }

        //
        // GET: /SpecDetails/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            SpecDetails specdetails = db.SpecDetails.Find(id);
            if (specdetails == null)
            {
                return HttpNotFound();
            }
            return View(specdetails);
        }

        //
        // GET: /SpecDetails/Create

        public ActionResult Create()
        {
            ViewBag.SpecID = new SelectList(db.Spec, "SpecID", "ModelName");
            return View();
        }

        //
        // POST: /SpecDetails/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SpecDetails specdetails)
        {
            if (ModelState.IsValid)
            {
                db.SpecDetails.Add(specdetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SpecID = new SelectList(db.Spec, "SpecID", "ModelName", specdetails.SpecID);
            return View(specdetails);
        }

        //
        // GET: /SpecDetails/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SpecDetails specdetails = db.SpecDetails.Find(id);
            if (specdetails == null)
            {
                return HttpNotFound();
            }
            ViewBag.SpecID = new SelectList(db.Spec, "SpecID", "ModelName", specdetails.SpecID);
            return View(specdetails);
        }

        //
        // POST: /SpecDetails/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SpecDetails specdetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specdetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SpecID = new SelectList(db.Spec, "SpecID", "ModelName", specdetails.SpecID);
            return View(specdetails);
        }

        //
        // GET: /SpecDetails/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SpecDetails specdetails = db.SpecDetails.Find(id);
            if (specdetails == null)
            {
                return HttpNotFound();
            }
            return View(specdetails);
        }

        //
        // POST: /SpecDetails/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpecDetails specdetails = db.SpecDetails.Find(id);
            db.SpecDetails.Remove(specdetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}