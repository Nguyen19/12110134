﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    public class XperiaController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /Xperia/

        public ActionResult Index()
        {
            return View(db.Xperia.ToList());
        }

        //
        // GET: /Xperia/Details/5

        public ActionResult Details(int id = 0)
        {
            Xperia xperia = db.Xperia.Find(id);
            if (xperia == null)
            {
                return HttpNotFound();
            }
            return View(xperia);
        }

        //
        // GET: /Xperia/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Xperia/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Xperia xperia)
        {
            if (ModelState.IsValid)
            {
                db.Xperia.Add(xperia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(xperia);
        }

        //
        // GET: /Xperia/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Xperia xperia = db.Xperia.Find(id);
            if (xperia == null)
            {
                return HttpNotFound();
            }
            return View(xperia);
        }

        //
        // POST: /Xperia/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Xperia xperia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(xperia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(xperia);
        }

        //
        // GET: /Xperia/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Xperia xperia = db.Xperia.Find(id);
            if (xperia == null)
            {
                return HttpNotFound();
            }
            return View(xperia);
        }

        //
        // POST: /Xperia/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Xperia xperia = db.Xperia.Find(id);
            db.Xperia.Remove(xperia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}