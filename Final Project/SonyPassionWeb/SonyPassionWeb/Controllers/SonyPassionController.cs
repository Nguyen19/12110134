﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    public class SonyPassionController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /SonyPassion/

        public ActionResult Index()
        {
            return View(db.SonyPassions.ToList());
        }

        //
        // GET: /SonyPassion/Details/5

        public ActionResult Details(int id = 0)
        {
            SonyPassion sonypassion = db.SonyPassions.Find(id);
            ViewData["idnewss"] = id;
            if (sonypassion == null)
            {
                return HttpNotFound();
            }
            return View(sonypassion);
        }

        //
        // GET: /SonyPassion/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SonyPassion/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SonyPassion sonypassion)
        {
            if (ModelState.IsValid)
            {
                db.SonyPassions.Add(sonypassion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sonypassion);
        }

        //
        // GET: /SonyPassion/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SonyPassion sonypassion = db.SonyPassions.Find(id);
            if (sonypassion == null)
            {
                return HttpNotFound();
            }
            return View(sonypassion);
        }

        //
        // POST: /SonyPassion/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SonyPassion sonypassion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sonypassion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sonypassion);
        }

        //
        // GET: /SonyPassion/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SonyPassion sonypassion = db.SonyPassions.Find(id);
            if (sonypassion == null)
            {
                return HttpNotFound();
            }
            return View(sonypassion);
        }

        //
        // POST: /SonyPassion/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SonyPassion sonypassion = db.SonyPassions.Find(id);
            db.SonyPassions.Remove(sonypassion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}