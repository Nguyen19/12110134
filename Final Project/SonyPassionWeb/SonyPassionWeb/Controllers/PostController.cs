﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    
    public class PostController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /Post/
        
        public ActionResult Index()
        {
            var post = db.Post.Include(p => p.Thread);
            return View(post.ToList());
        }

        //
        // GET: /Post/Details/5
       
        public ActionResult Details(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.ThreadID = new SelectList(db.Thread, "ThreadID", "TitleOfThread");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, int idthread)
        {
            if (ModelState.IsValid)
            {
                post.DateOfPost = DateTime.Now;
                post.ThreadID = idthread;
                db.Post.Add(post);
                db.SaveChanges();
                return PartialView("viewPost", post);
                //return RedirectToAction("Details/" + idthread, "Thread");
            }

            ViewBag.ThreadID = new SelectList(db.Thread, "ThreadID", "TitleOfThread", post.ThreadID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.ThreadID = new SelectList(db.Thread, "ThreadID", "TitleOfThread", post.ThreadID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ThreadID = new SelectList(db.Thread, "ThreadID", "TitleOfThread", post.ThreadID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Post.Find(id);
            db.Post.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}