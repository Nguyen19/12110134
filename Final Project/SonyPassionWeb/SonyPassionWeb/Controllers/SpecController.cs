﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    [Authorize]
    public class SpecController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /Spec/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Spec.ToList());
        }

        //
        // GET: /Spec/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Spec spec = db.Spec.Find(id);
            if (spec == null)
            {
                return HttpNotFound();
            }
            return View(spec);
        }

        //
        // GET: /Spec/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Spec/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Spec spec)
        {
            if (ModelState.IsValid)
            {
                db.Spec.Add(spec);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(spec);
        }

        //
        // GET: /Spec/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Spec spec = db.Spec.Find(id);
            if (spec == null)
            {
                return HttpNotFound();
            }
            return View(spec);
        }

        //
        // POST: /Spec/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Spec spec)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spec).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(spec);
        }

        //
        // GET: /Spec/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Spec spec = db.Spec.Find(id);
            if (spec == null)
            {
                return HttpNotFound();
            }
            return View(spec);
        }

        //
        // POST: /Spec/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Spec spec = db.Spec.Find(id);
            db.Spec.Remove(spec);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}