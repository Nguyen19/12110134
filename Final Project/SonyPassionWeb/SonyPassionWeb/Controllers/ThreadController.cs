﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SonyPassionWeb.Models;

namespace SonyPassionWeb.Controllers
{
    [Authorize]
    public class ThreadController : Controller
    {
        private SonyPassionDbContext db = new SonyPassionDbContext();

        //
        // GET: /Thread/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var thread = db.Thread.Include(t => t.Forum).Include(t => t.UserProfile);
            return View(thread.ToList());
        }

        //
        // GET: /Thread/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Thread thread = db.Thread.Find(id);
            ViewData["idthread"] = id;
            if (thread == null)
            {
                return HttpNotFound();
            }
            return View(thread);
        }

        //
        // GET: /Thread/Create

        public ActionResult Create()
        {
            ViewBag.ForumID = new SelectList(db.Forum, "ForumID", "ForumModel");
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Thread/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Thread thread)
        {
            if (ModelState.IsValid)
            {
                thread.DateOfThread = DateTime.Now;
                thread.LastTimeEdited = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                thread.UserProfileUserId = userid;
                db.Thread.Add(thread);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ForumID = new SelectList(db.Forum, "ForumID", "ForumModel", thread.ForumID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", thread.UserProfileUserId);
            return View(thread);
        }

        //
        // GET: /Thread/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Thread thread = db.Thread.Find(id);
            if (thread == null)
            {
                return HttpNotFound();
            }
            ViewBag.ForumID = new SelectList(db.Forum, "ForumID", "ForumModel", thread.ForumID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", thread.UserProfileUserId);
            return View(thread);
        }

        //
        // POST: /Thread/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Thread thread)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thread).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ForumID = new SelectList(db.Forum, "ForumID", "ForumModel", thread.ForumID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", thread.UserProfileUserId);
            return View(thread);
        }

        //
        // GET: /Thread/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Thread thread = db.Thread.Find(id);
            if (thread == null)
            {
                return HttpNotFound();
            }
            return View(thread);
        }

        //
        // POST: /Thread/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Thread thread = db.Thread.Find(id);
            db.Thread.Remove(thread);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}